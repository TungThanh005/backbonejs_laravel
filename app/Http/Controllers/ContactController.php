<?php

namespace App\Http\Controllers;

use App\Contacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller
{
    public function index()
    {
        return Contacts::all();
    }
    public function store(ContactRequest $request)
    {
        Contacts::create(array(
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email_address'=>$request->email_address,
            'description'=>$request->description,
        ));
    }
    public function update(Request $request,$id)
    {
            $con=Contacts::find($id);
            $con->first_name=$request->first_name;
            $con->last_name=$request->last_name;
            $con->email_address=$request->email_address;
            $con->description=$request->description;
            $con->save();
    }
    public function destroy($id)
    {
         Contacts::find($id)->delete();

    }
}
