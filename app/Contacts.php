<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table='contacts';
    protected $fillable = [
        'first_name','last_name','email_address','description',
    ];
    public $timestamps = false;
}
