<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Contacts</h1>
<form id="addContact" action="">
    <div>
        <lable for="first_name">first_name</lable>
        <input type="text" id="first_name" name="first_name">
    </div>
    <div>
        <lable for="last_name" >last_name</lable>
        <input type="text" id="last_name" name="last_name">
    </div>
    <div>
        <lable for="enail_address">enail_address</lable>
        <input type="text" id="email_address" name="enail_address">
    </div>
    <div>
        <lable for="description">description</lable>
        <input type="text" id="description" name="description">
    </div>
    <div>
        <input type="submit" value="okie">
    </div>
</form>
<h1>list COntact</h1>
<table id="tableContact">
    <thead>
        <tr>
            <th>first_name </th>
            <th>last_name  </th>
            <th>email_address </th>
            <th>description</th>
        </tr>
    </thead>
</table>
<div class="editcontact"></div>
<script id="editContact" type="text/template">
    <h1>Edit Contact</h1>
    <form id="editContact" action="">
        <div>
        <lable for="edit_first_name">edit_first_name</lable>
        <input type="text" id="edit_first_name" name="edit_first_name" value="<%= first_name %>">
        </div>
        <div>
        <lable for="edit_last_name" >edit_last_name</lable>
        <input type="text" id="edit_last_name" name="edit_last_name" value="<%= last_name %>">
        </div>
        <div>
        <lable for="edit_email_address">edit_email_address</lable>
        <input type="text" id="edit_email_address" name="edit_enail_address" value="<%= email_address %>"
        </div>
        <div>
        <lable for="edit_description">edit_description</lable>
        <input type="text" id="edit_description" name="edit_description" value="<%= description %>"
        </div>
        <div>
        <input type="submit" value="edit">
        </div>
        </form>
</script>
<script id="AllContact" type="text/template">
    <td><%= first_name %></td>
     <td><%= last_name %></td>
     <td><%= email_address %></td>
     <td><%= description %></td>
     <td><a href="#contacts/<%= id %>/edit" class="edit">edit</a></td>
     <td><a href="#contacts/<%= id %>" class="delete">Delete</a></td>
</script>
<script src="{!! asset('js/jquery-3.3.1.min.js') !!}" ></script>
<script src="{!! asset('js/underscore-min.js') !!}" ></script>
<script src="{!! asset('js/backbone-min.js') !!}" ></script>
<script src="{!! asset('js/my.js') !!}" ></script>
<script src="{!! asset('js/models.js') !!}" ></script>
<script src="{!! asset('js/collections.js') !!}" ></script>
<script src="{!! asset('js/views.js') !!}" ></script>
<script src="{!! asset('js/router.js') !!}" ></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    new App.Router;
    Backbone.history.start();
    App.contacts=new App.Collection.Contacts;
    App.contacts.fetch().then(function () {
        new App.Views.App({ collection: App.contacts });
    });
</script>



</body>
</html>