
App.Views.App=Backbone.View.extend({
    initialize:function () {
        vent.on('contact:eidt', this.editContact, this);
       var addContactView=new App.Views.AddContact({ collection: App.contacts });
        var allContactView= new App.Views.Contacts({ collection: App.contacts }).render();
        $('#tableContact').append(allContactView.el);
        },
    editContact:function(contact){
        var editContactView = new App.Views.EditContact({ model: contact });
        $('.editcontact').html(editContactView.el);
    }
});
App.Views.AddContact=Backbone.View.extend({
    el: '#addContact',
    events:{
        'submit':'addContact'
    },
    addContact:function (e) {
        e.preventDefault();
        this.collection.create({
            first_name:this.$('#first_name').val(),
            last_name:this.$('#last_name').val(),
            email_address:this.$('#email_address').val(),
            description:this.$('#description').val(),
        },{wait:true});
        alert('Add new success');
        location.reload();
    }
});

App.Views.Contacts=Backbone.View.extend({
    tagName:"tbody",
    initialize:function(){
      this.collection.on('sync', this.addOne);
    },
    render:function () {
    this.collection.each(this.addOne, this);
    return this;
    },
    addOne:function(contact) {
        var contactView= new App.Views.Contact({ model:contact });
        this.$el.append(contactView.render().el)
    }
});
App.Views.Contact=Backbone.View.extend({
    tagName:'tr',
    template:template('AllContact'),
    initialize:function(){
      this.model.on('destroy', this.unrender, this);
      this.model.on('change', this.render, this);
    },
    events:{
      'click a.delete':'deleteContact',
      'click a.edit':'editContact'
    },
    editContact:function(){
       vent.trigger('contact:eidt', this.model);

    },
    deleteContact:function(){
      this.model.destroy();
    },

    render:function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
    unrender:function () {
        this.remove();
    }
});
App.Views.EditContact=Backbone.View.extend({
    template:template('editContact'),
    initialize:function(){
      this.render();
    },
    events:{
      'submit':'submit'
    },
    submit:function(e){
        e.preventDefault();
        this.model.save({
            first_name: $('#edit_first_name').val(),
            last_name: $('#edit_last_name').val(),
            email_address: $('#edit_email_address').val(),
            description: $('#edit_description').val(),
        });
        this.remove();
    },
    render:function () {
        var html= this.template( this.model.toJSON());
        this.$el.html(html);
        return this;
    }

});
