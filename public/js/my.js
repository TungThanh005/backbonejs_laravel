(function () {
    window.App={
      Models:{},
      Collection:{},
      Views:{},
      Router:{}
    };
    window.vent=_.extend({},Backbone.Events);
    window.template=function (id) {
        return _.template($('#'+id).html());
    }
})();